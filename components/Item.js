import React, { Component } from 'react';
import { TouchableHighlight, View, Text } from 'react-native';
import styles from '../styles.js';

import AppText from './AppText';
import CurrencyText from './CurrencyText';

export default class Item extends Component {
  render() {
    return (
      <TouchableHighlight
        onPress={ this.props.onPress }>
        {/* Child of touchable must be a single view for text */}
        <View style={ styles.item }>
          <View style={ styles.itemDetails }>
            <AppText max={ 34 }
              style={ this.props.checked ? styles.itemCheckedText : styles.itemText }>
              { this.props.children }
            </AppText>

            <View style={ styles.rowFlex }>
              <CurrencyText style={ styles.itemDetailText }>
                { this.props.price }
              </CurrencyText>
                <AppText max={ 30 }
                  style={ styles.itemDetailText, styles.note }>
                { this.props.note }
                </AppText>
            </View>

          </View>

          <View style={ this.props.checked ? styles.quantityBoxChecked : styles.quantityBox }>
            <Text style={ styles.quantity }>{ this.props.quantity }</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}
