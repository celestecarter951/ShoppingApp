import React, { Component } from 'react';
import { Text } from 'react-native';
import styles from '../styles.js';

export default class AppText extends Component {
  render() {
    let maxlimit = this.props.max;
    let style = this.props.style || styles.defaultFont;
    let giveMaxLimit = (maxlimit) ? (this.props.children).length > maxlimit : 0;
    return (
      <Text style={ style }>
        {
          (giveMaxLimit) ?
          (((this.props.children).substring(0,maxlimit-3)) + '...') :
          this.props.children
        }
      </Text>
    );
  }
}
