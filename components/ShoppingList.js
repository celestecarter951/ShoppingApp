import React, { Component } from 'react';
import { ListView, View, Text, TextInput } from 'react-native';
import styles from '../styles.js';

import ActionButton from 'react-native-circular-action-menu';
import AddItem from './AddItem';
import Category from './Category';
import Dialog from './Dialog';
import ListOptions from './ListOptions';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Item from './Item';
import ItemViewer from './ItemViewer';
import Title from './Title';
import { SwipeListView } from 'react-native-swipe-list-view';

import _ from 'lodash';
import db from '../db';

export default class ShoppingList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: 0,
      mode: false,
      sort: false
    };

    this.closeModal = this.closeModal.bind(this);
    this.editItem = this.editItem.bind(this);
    this.getModal = this.getModal.bind(this);
    this.rename = this.rename.bind(this);
    this.delete = this.delete.bind(this);
    this.sortList = this.sortList.bind(this);
    this.getList = this.getList.bind(this);
    this.listOptions = this.listOptions.bind(this);
    this.sortByCategories = this.sortByCategories.bind(this);
    this.sortByAll = this.sortByAll.bind(this);
  }

  editItem(id) {
    this.setState({ mode: 'item', index: parseInt(id) });
  }

  rename() {
    this.setState({ mode: 'rename' });
  }

  delete() {
    this.setState({ mode: 'delete' });
  }

  closeModal() {
    this.setState({ mode: false });
  }

  listNameOnChange(name) {
    this.name = name;
  }

  listNameSubmit() {
    return db.list.update.name(this.name);
  }

  listOptions() {
    this.setState({ mode: 'listOptions'});
  }

  sortByCategories() {
    this.setState({ sort: false });
    this.closeModal();
  }

  sortByAll() {
    this.setState({ sort: 'all' });
    this.closeModal();
  }

  getModal(data) {
    switch (this.state.mode) {
      case 'item':
        return (<ItemViewer
                  categories={ this.props.categories }
                  close={ this.closeModal }
                  index={ this.state.index }
                  items={ data }
                  lists={ this.props.lists }
                  name={ this.props.name }
                />);
      case 'rename':
        let context = {
          name: this.props.name
        };
        return (<Dialog
                  title={ "Edit list name" }
                  cancelBtn={ "CANCEL"}
                  doneBtn={ "EDIT" }
                  submit={ () => this.listNameSubmit.bind(context)()
                    .then(this.closeModal) }
                  close={ this.closeModal }>
                  <TextInput
                    onChangeText={ this.listNameOnChange.bind(context) }
                    defaultValue={ this.props.name }/>
                </Dialog>);
      case 'delete':
        return (<Dialog
                  title={ "Delete shopping list?" }
                  cancelBtn={ "CANCEL"}
                  doneBtn={ "DELETE" }
                  submit={ () => db.list.delete()
                    .then(this.closeModal) }
                  close={ this.closeModal }>
                  <View>
                    <Text>Are you sure you want to delete this shopping list?</Text>
                    <Text>All items in this list will be deleted.</Text>
                    <Text>Others you shared this list with, won't be able to access this list after it's been deleted.</Text>
                  </View>
                </Dialog>);
      case 'listOptions':
        return (<ListOptions
                  close={ this.closeModal }
                  sortByCategories={ this.sortByCategories }
                  sortByAll={ this.sortByAll }
                />)
      default:
        return null;
    }
  }

  sortList (items) {
    let source;

    if (_.keys(items).length == 0) {
      source = new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      });
      return {
        sorted: [],
        source: source.cloneWithRows([])
      }
    }

    switch (this.state.sort) {
      case 'all':
        source = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2
        });
        let half = _.groupBy(items, 'checked');
        let undone = _.get(half, false, []);
        let done = _.get(half, true, []);
        let sorted = _.map(undone.concat(done), (item, index) => {
          item.index = index;
          return item;
        })
        return {
          sorted: sorted,
          source: source.cloneWithRows(sorted)
        };
      // categories by default
      default:
        source = new ListView.DataSource({
          rowHasChanged: (r1, r2) => r1 !== r2,
          sectionHeaderHasChanged: (s1, s2) => s1 !== s2,
          getSectionHeaderData: (dataBlob, sectionId) =>  ({ category: sectionId }),
          getRowData: (dataBlob, sectionId, rowId) => dataBlob[sectionId][rowId]
        });
        let split = _.groupBy(items, 'checked');
        let checked = split.true;
        let groupedItems = _.groupBy(split.false, 'category');
        let headers = _.keys(groupedItems);
        if (!_.isUndefined(split.true)) {
          headers = headers.concat(['checked']);
          groupedItems.checked = checked;
        }
        let sortedItems = _.flatten(_.map(headers, (header) => {
          return _.get(groupedItems, header, []);
        }));

        _.each(sortedItems, (item, index) => {
          // Items in sortedItems are a reference to groupedItems
          sortedItems[index].index = index;
        })
        return {
          sorted: sortedItems,
          source: source.cloneWithRowsAndSections(groupedItems, headers)
        };
    }
  }

  getList (source) {
    switch (this.state.sort) {
      case 'all':
        return (
          <SwipeListView
            enableEmptySections={ true }
            dataSource={ source }
            renderRow={ (item, sectionId, rowId) =>
              <Item {...item}
                onPress={() => this.editItem(item.index)}>
                    { item.name }
              </Item>}
            disableRightSwipe={true}
            rightOpenValue={-50}
            onRowOpen={(secId, rowId, rows) => {
              let items = Object.keys(this.props.items)
                .map((key) => {
                  let item = this.props.items[key];
                  return Object.assign({ name: key }, item);
                });
              let half = _.groupBy(items, 'checked');
              let undone = _.get(half, false, []);
              let done = _.get(half, true, []);
              let all = undone.concat(done);
              let curItem = all[rowId];

              rows[`${secId}${rowId}`].closeRow();
              db.item.update(curItem.name, {checked: !curItem.checked});
            }}
            renderHiddenRow={ data => (
                <View style={{
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between', paddingRight: 15}}>
                    <Icon name="done" style={ styles.check }/>
                </View>
            )}
          />
        )
      default:
        return (
          <SwipeListView
            enableEmptySections={ true }
            dataSource={ source }
            renderSectionHeader={(sectionData) => <Category {...sectionData} />}
            renderRow={ (item, sectionId, rowId) =>
              <Item {...item}
                onPress={() => this.editItem(item.index)}>
                    { item.name }
              </Item>}
            disableRightSwipe={true}
            rightOpenValue={-50}
            onRowOpen={(secId, rowId, rows) => {
              let items = _(this.props.items)
                .keys()
                .map((key) => {
                  let item = _.clone(this.props.items[key]);
                  if (item.checked) {
                    item.category = 'checked'
                  }
                  return Object.assign({ name: key }, item);
                });

              let checked = secId == 'checked';
              let curItem = _(items)
                .groupBy('category')
                .pick(secId)
                .values()
                .flatten()
                .filter({ checked })
                .at(rowId)
                .head();

              rows[`${secId}${rowId}`].closeRow();
              db.item.update(curItem.name, {checked: !checked});
            }}
            renderHiddenRow={ data => (
                <View style={{
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between', paddingRight: 15}}>
                    <Icon name="done" style={ styles.check }/>
                </View>
            )}
          />
        )
    }
  }

  render() {
    let listName = this.props.name;
    let items = Object.keys(this.props.items)
      .map((key) => {
        let item = this.props.items[key];
        return Object.assign({ name: key }, item);
      });

    let list = this.sortList(items);
    return (
      <View style={ styles.mainView }>

        <Title
          text={ listName }
          openControlPanel={ this.props.openControlPanel }
          listOptions={ this.listOptions }
        />
        <AddItem items={ items }/>

        <View style={ styles.itemList }>

          {this.getList(list.source)}

          {this.getModal(list.sorted)}

        </View>

        <ActionButton
          buttonColor="rgba(214,53,74,1)"
          position="right"
          btnOutRange="rgba(40,39,49,1)"
          bgColor="rgba(0,0,0,0.5)">
          <ActionButton.Item
            size={42}
            buttonColor='#D93344'
            title="Edit"
            onPress={ this.rename }>
            <Icon name="edit" style={ styles.storeIcons }/>
          </ActionButton.Item>
          <ActionButton.Item
            size={42}
            buttonColor='#D93344'
            title="Delete"
            onPress={ this.delete }>
            <Icon name="delete" style={ styles.storeIcons }/>
          </ActionButton.Item>
          <ActionButton.Item
            size={42}
            buttonColor='#D93344'
            title="Share"
            onPress={() => console.warn("Share list pressed")}>
            <Icon name="share" style={ styles.storeIcons }/>
          </ActionButton.Item>
        </ActionButton>

      </View>
    )
  }
}
