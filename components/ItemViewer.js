import React, { Component } from 'react';
import { Modal, View, ViewPagerAndroid } from 'react-native';

import ItemDetail from './ItemDetail';

export default class ItemViewer extends Component {
  render() {
    let pages = this.props.items
      .map((item) => {
        return (
          <View key={ item.index } >
            <ItemDetail {...item}
              storeName={ this.props.name }
              lists={ this.props.lists }
              close={ this.props.close }
              categories={ this.props.categories } />
          </View>
        );
      });

    return (
      <Modal
        animationType={'slide'}
        onRequestClose={this.props.close} >
        <ViewPagerAndroid
          initialPage={ this.props.index }
          style={{ flex: 1 }}>
          { pages }
        </ViewPagerAndroid>
      </Modal>
    )
  }
}
