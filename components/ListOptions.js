import React, { Component } from 'react';
import { Modal, Text, TouchableWithoutFeedback, View } from 'react-native';
import styles from '../styles.js';

export default class ListOptions extends Component {
  render() {
    return (
      <Modal
        transparent={ true }
        animationType={ 'fade' }
        onRequestClose={ this.props.close } >
        <TouchableWithoutFeedback onPress={ this.props.close }>
        <View
          style={ styles.listOptionsModal }>
          <View
            style={ styles.popupList }>
            <TouchableWithoutFeedback>
            <View
              style={ styles.modalContent }>
              <Text style={ styles.listOption }
                onPress={this.props.sortByCategories}>
                Sort by categories
              </Text>
              <Text style={ styles.horizontalLine }></Text>
              <Text style={ styles.listOption }
                onPress={this.props.sortByAll}>
                Sort by all items
              </Text>
            </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}
