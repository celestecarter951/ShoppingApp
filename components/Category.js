import React, { Component } from 'react';
import { View, Text } from 'react-native';
import styles from '../styles.js';

export default class Category extends Component {
  render() {
    return(
      <View style={ styles.category }>
        <Text style={ styles.categoryText }>{this.props.category}</Text>
      </View>
    );
  }
}
