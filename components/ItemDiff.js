import React, { Component } from 'react';

import {
  Text,
  View } from 'react-native';
import AppText from './AppText';

import styles from '../styles.js';

export default class ItemDiff extends Component {
  calculatePricePerMeasurement(itemPrice, measurementAmount) {
    const price = parseFloat(itemPrice) || "0.00";
    const amount = parseFloat(measurementAmount) || "1.0";
    const ppm = price / amount;
    return ppm.toFixed(2);
  }

  render() {
    let colStyle = {
      flexDirection: 'column',
      flex: 1
    };

    const currentPricePerMeasurement = this.calculatePricePerMeasurement(this.props.current.price, this.props.current.measurementAmount);
    const otherPricePerMeasurement = this.calculatePricePerMeasurement(this.props.other.price, this.props.other.measurementAmount);

    return (<View style={{flexDirection: 'row'}}>
  <View style={colStyle}>
    <AppText max={ 26 }
      style={[ styles.boldFont, { textAlign: 'center', color: 'white'}]}>
      { this.props.current.name }
    </AppText>
    <AppText max={ 20 }
      style={ styles.defaultFont }>
      { this.props.current.store }
    </AppText>
    <AppText max={ 20 }
      style={ styles.defaultFont }>
      price: ${ this.props.current.price }
    </AppText>
    <AppText max={ 20 }
      style={ styles.defaultFont }>
      { this.props.current.measurementAmount } { this.props.current.measurement }
    </AppText>
    <AppText max={ 20 }
      style={[ styles.defaultFont, {borderTopColor: '#53DBD0', borderTopWidth: 0.5, paddingTop: 8, textAlign: 'center'} ]}>
      Price per { this.props.current.measurement }:
    </AppText>
    <AppText max={ 20 }
      style={[ styles.boldFont, {textAlign: 'center', color: 'white'} ]}>
      ${ currentPricePerMeasurement }
    </AppText>
  </View>
  <View style={[colStyle, {borderLeftColor: '#53DBD0', borderLeftWidth: 0.5, paddingLeft: 5}]}>
    <AppText max={ 26 }
      style={[ styles.boldFont, { textAlign: 'center', color: 'white'}]}>
      { this.props.other.name }
    </AppText>
    <AppText max={ 20 }
      style={ styles.defaultFont }>
      { this.props.other.store }
    </AppText>
    <AppText max={ 20 }
      style={ styles.defaultFont }>
      price: ${ this.props.other.price }
    </AppText>
    <AppText max={ 20 }
      style={ styles.defaultFont }>
      { this.props.other.measurementAmount } { this.props.other.measurement }
    </AppText>
    <AppText max={ 20 }
      style={[ styles.defaultFont, {borderTopColor: '#53DBD0', borderTopWidth: 0.5, paddingTop: 8, textAlign: 'center'} ]}>
      Price per { this.props.other.measurement }:
    </AppText>
    <AppText max={ 20 }
      style={[ styles.boldFont, {textAlign: 'center', color: 'white'} ]}>
      ${ otherPricePerMeasurement }
    </AppText>
  </View>
    </View>);
  }
}
