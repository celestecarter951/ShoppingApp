import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  ToastAndroid,
  Picker } from 'react-native';
import Dialog from './Dialog';
import ItemDiff from './ItemDiff';
import AppText from './AppText'

import styles from '../styles.js';
import Icon from 'react-native-vector-icons/MaterialIcons';

import _ from 'lodash';
import db from '../db';

export default class ItemDetail extends Component {
  constructor(props) {
    super(props);

    let list = _(this.props.lists).keys().head();
    let item = _(db.cached.list(list).items())
      .keys()
      .first();

    this.state = {
      mode: false,
      selected: { list, item },
      ...this.props
    }

    this.handleItemDelete = this.handleItemDelete.bind(this);
    this.decreaseQuantity = this.decreaseQuantity.bind(this);
    this.increaseQuantity = this.increaseQuantity.bind(this);
    this.calculatePricePerMeasurement = this.calculatePricePerMeasurement.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.category = this.category.bind(this);
    this.delete = this.delete.bind(this);
    this.copy = this.copy.bind(this);
    this.compare = this.compare.bind(this);
    this.getModal = this.getModal.bind(this);
    this.handleName = this.handleName.bind(this);
    this.updateSelectedList = this.updateSelectedList.bind(this);
    this.currentItem = this.currentItem.bind(this);
    this.compareItemToAnother = this.compareItemToAnother.bind(this);
  }

  currentItem () {
    return _.pick(this.state, [
      'category',
      'measurement',
      'measurementAmount',
      'name',
      'note',
      'price',
      'quantity'
    ]);
  }

  updateSelectedList (list) {
    let item = _(db.cached.list(list).items())
      .keys()
      .first();

    this.setState({
      selected: { list, item }
    });
  }

  handleItemDelete() {
    this.closeModal();
    this.props.close();
    return db.item.delete(this.state.name);
  }

  handleItemCopy() {
    let item = this.currentItem();

    return db.item.copy(item, this.state.selected.list);
  }

  compareItemToAnother() {
    this.setState({ mode: 'diff' });
  }

  handleName(event) {
    let name = event.nativeEvent.text;
    name = name === '' ? this.state.name : name;

    this.setState({name});
  }

  decreaseQuantity() {
    const qty = parseInt(this.state.quantity) - 1;
    if (qty > 0) {
      this.setState({quantity: qty})
    } else if (qty == 0) {
      ToastAndroid.show("Item has been crossed off your list", ToastAndroid.LONG);
      db.item.update(this.state.name, {checked: true});
      this.setState({quantity: qty})
    } else if (qty < 0) {
      ToastAndroid.show("Negative quantities aren't allowed", ToastAndroid.LONG);
    }
  }

  increaseQuantity() {
    const qty = parseInt(this.state.quantity) + 1;

    if (qty > 0 && this.state.quantity == 0) {
      ToastAndroid.show("Item has been added to your list", ToastAndroid.LONG);
      db.item.update(this.state.name, {checked: false});
    }

    if (qty < 1000) {
      this.setState({quantity: qty})
    }
  }

  calculatePricePerMeasurement() {
    const price = parseFloat(this.state.price) || "0.00";
    const amount = parseFloat(this.state.measurementAmount) || "1.0";
    const ppm = price / amount;
    return ppm.toFixed(2);
  }

  closeModal() {
    this.setState({ mode: false });
  }

  category() {
    this.setState({ mode: 'category' });
  }

  delete() {
    this.setState({ mode: 'delete' });
  }

  copy() {
    this.setState({ mode: 'copy' });
  }

  compare() {
    this.setState({ mode: 'compare' });
  }

  categoryOnChange(categoryName) {
    this.categoryName = categoryName;
  }

  categorySubmit() {
    return db.categories.add(this.categoryName);
  }

  getModal() {
    let stores = Object.keys(this.props.lists)
      .map((id, index) => {
        return <Picker.Item
                  key={index}
                  value={id}
                  label={this.props.lists[id] }
                />
      });

    switch (this.state.mode) {
      case 'category':
        let context = {
          categoryName: this.props.category
        };
        return (<Dialog
                  title={ "Create a category"}
                  cancelBtn={ "CANCEL" }
                  doneBtn={ "CREATE" }
                  submit={ () => this.categorySubmit.bind(context)()
                         .then(this.closeModal) }
                  close={ this.closeModal }>
                  <TextInput
                    onChangeText={ this.categoryOnChange.bind(context) }
                    placeholder="Category Name"/>
                </Dialog>);
      case 'delete':
        return (<Dialog
                  title={ "Delete item?" }
                  cancelBtn={ "CANCEL" }
                  doneBtn={ "DELETE "}
                  submit={ this.handleItemDelete }
                  close={ this.closeModal }>
                  <View>
                    <AppText>Are you sure you want to delete this item?</AppText>
                    <AppText>
                      The item '{ this.state.name }'
                      will be remove from the current list.
                    </AppText>
                  </View>
                </Dialog>);
      case 'copy':
        return (<Dialog
                  title={ "Copy item" }
                  cancelBtn={ "CANCEL " }
                  doneBtn={ "COPY" }
                  submit={ () => this.handleItemCopy()
                    .then(this.closeModal) }
                  close={ this.closeModal }>
                  <View>
                    <AppText>Where do you want to put your copy?</AppText>
                    <Picker
                      selectedValue={this.state.selected.list}
                      onValueChange={this.updateSelectedList}
                      prompt="Copy to store">
                      { stores }
                    </Picker>
                  </View>
                </Dialog>);
      case 'compare':
        let cachedItems = db.cached.list(this.state.selected.list).items();
        let items = Object.keys(cachedItems)
          .map((name, index) => {
            return <Picker.Item
                      key={index}
                      value={name}
                      label={name}
                    />
          })
        return (<Dialog
                  title={ "Compare item" }
                  cancelBtn={ "CANCEL " }
                  doneBtn={ "COMPARE" }
                  submit={ () => this.compareItemToAnother() }
                  close={ this.closeModal }>
                  <View>
                    <AppText>Choose a store:</AppText>
                    <Picker
                      selectedValue={this.state.selected.list}
                      onValueChange={this.updateSelectedList}>
                      { stores }
                    </Picker>
                    <AppText>Now pick an item to compare with:</AppText>
                    <Picker
                      onValueChange={(item) => this.setState({
                          selected: {
                            list: this.state.selected.list,
                            item
                          }})}
                      selectedValue={ this.state.selected.item }>
                      { items }
                    </Picker>
                  </View>
                </Dialog>);
      case 'diff':
        let currentItem = this.currentItem();
        let otherItem = db.cached.list(this.state.selected.list)
          .items()[this.state.selected.item];

        otherItem.name = this.state.selected.item;
        otherItem.store = this.props.lists[this.state.selected.list];
        currentItem.store = this.props.storeName

        return (<Dialog
                  title={ "Compare item" }
                  cancelBtn={ "BACK" }
                  doneBtn={ "DONE" }
                  submit={ this.closeModal }
                  close={ this.compare }>
                  <ItemDiff current={currentItem} other={otherItem}/>
                </Dialog>);
      default:
        return null;
    }
  }

  render() {
    const name = this.state.name;
    const price = this.state.price;
    const note = this.state.note;
    const quantity = this.state.quantity;
    const measurementAmount = this.state.measurementAmount;
    const measurement = this.state.measurement;
    const pricePerMeasurement = this.calculatePricePerMeasurement();
    const category = this.state.category;

    return (
      <View style={ styles.mainView }>

        <View style={ styles.rowFlex }>
          <View style={[ styles.topButton, styles.deleteItem ]}>
            <Text
              style={ styles.topButtonText }
              onPress={ this.delete }>
              Delete
            </Text>
          </View>
          <View style={[ styles.topButton, styles.moveItem ]}>
            <Text
              style={ styles.topButtonText }
              onPress={ this.copy }>
              Copy
            </Text>
          </View>
          <View style={[ styles.topButton, styles.copyItem ]}>
            <Text
              style={ styles.topButtonText }
              onPress={ this.compare }>
              Compare
            </Text>
          </View>
        </View>

        <ScrollView>
          <View style={[ styles.space, {flex: 1} ]}>
            <TextInput
              style={ styles.input }
              placeholderStyle={ styles.input }
              onEndEditing={ this.handleName }
              onSubmitEditing={ this.handleName }
              placeholder="Name"
              defaultValue={ name }
              autoCapitalize={"sentences"} />

            <View style={ styles.rowFlex}>
              <Text style={ styles.currencySign }>$</Text>
              <TextInput
                style={[ styles.itemDetails, styles.input ]}
                onChangeText={ (text) => this.setState({price: text}) }
                placeholder="Price"
                value={ price }
                keyboardType="numeric" />
            </View>

            <View style={[ styles.rowFlex, styles.rowSpacing ]}>
              <TextInput
                style={[ styles.itemDetails, styles.measurementAmount ]}
                onChangeText={ (text) => this.setState({measurementAmount: text}) }
                placeholder="Measurement"
                value={ measurementAmount }
                keyboardType="numeric" />
              <Picker
                style={ styles.dropdown }
                prompt="Unit Measurement"
                selectedValue={ measurement }
                onValueChange={ (unit) => this.setState({measurement: unit}) }>
                <Picker.Item label="Measurement" value="" />
                <Picker.Item label="oz" value="oz" />
                <Picker.Item label="lbs" value="lbs" />
                <Picker.Item label="cup" value="cup" />
              </Picker>
            </View>

            <Text style={ styles.pricePerMeasurement }>
              Price per { measurement }: { pricePerMeasurement }
            </Text>

            <TextInput style={ styles.input }
              onChangeText={ (text) => this.setState({note: text}) }
              placeholder="Notes"
              value={ note }
              autoCapitalize={"sentences"} />

            <View style={ styles.rowFlex }>
              <View style={[ styles.editBtn, styles.subBtn ]}>
                <Icon.Button name="remove"
                  color="#44A3AC"
                  backgroundColor="rgba(0,0,0,0)"
                  iconStyle={ styles.iconBtn }
                  onPress={ this.decreaseQuantity } />
              </View>
              <Text style={ styles.quantity }>{ quantity }</Text>
              <View style={[ styles.editBtn, styles.addBtn ]}>
                <Icon.Button name="add"
                  color="#44A3AC"
                  backgroundColor="rgba(0,0,0,0)"
                  iconStyle={ styles.iconBtn }
                  onPress={ this.increaseQuantity } />
              </View>
            </View>

            <View style={ styles.rowFlex }>
              <Picker
                style={ styles.dropdown }
                prompt="Category"
                selectedValue={ category }
                onValueChange={ (type) => this.setState({category: type}) }>
                {this.props.categories.map((cat, index) => {
                  return (
                    <Picker.Item
                      key={index}
                      label={cat}
                      value={cat} />
                  );
                })}
              </Picker>
              <View style={ styles.editBtn }>
                <Icon.Button name="create"
                  color="#44A3AC"
                  backgroundColor="rgba(0,0,0,0)"
                  iconStyle={ styles.iconBtn }
                  onPress={ this.category } />
              </View>
            </View>

            { this.getModal() }

          </View>
        </ScrollView>
      </View>

    );
  }

  componentDidUpdate(_prevProps, prevState) {
    if (prevState.name != this.state.name) {
      db.item.rename(prevState.name, this.state.name);
      return;
    }

    let whitelist = [
      'category',
      'measurement',
      'measurementAmount',
      'note',
      'price',
      'quantity'
    ];
    let changes = _.reduce(this.state, (acc, val, key) => {
      if (val != prevState[key] && _.includes(whitelist, key)) {
        acc[key] = val
      }
      return acc;
    }, {});

    if (_.keys(changes).length > 0) {
      db.item.update(this.state.name, changes);
    }
  }
}
