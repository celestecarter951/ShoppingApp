import React, { Component } from 'react';
import { Text } from 'react-native';
import styles from '../styles.js';

export default class CurrencyText extends Component {
  render() {
    return (
      <Text style={this.props.style}>
        {
          ((this.props.children).length > 0) ? ('$' + this.props.children) : this.props.children
        }
      </Text>
    );
  }
}
