import React, { Component } from 'react';
import { View, TextInput, Button, Keyboard } from 'react-native';
import styles from '../styles.js';
import db from '../db';
import Icon from 'react-native-vector-icons/MaterialIcons';
import TextField from 'react-native-md-textinput';

export default class AddStore extends Component {
  constructor(props) {
    super(props);

    this.state = {
        storeName: ''
    };

    this._addStore = this._addStore.bind(this);
  }

  _addStore = () => {
    if (this.state.storeName) {
      db.list.add(this.state.storeName);
      this.setState({storeName: ''});
    }
    Keyboard.dismiss();
  }

  render() {
    let defaultText = 'OpenSans-Semibold';

    return(
      <View style={[ styles.rowFlex, styles.addItemView ]}>
      <TextField
        labelStyle={{ fontFamily: defaultText } }
        wrapperStyle={{ flex: 1 }}
        inputStyle={{ fontFamily: defaultText }}
        textColor={"white"}
        onChangeText={(text) => { this.state.storeName = text }}
        onSubmitEditing={ this._addStore }
        label={"Add store"}
        highlightColor={"#53DBD0"}
        maxLength={ 40 }
        value={ this.state.storeName }
        height={ 40 }
        autoCapitalize={"sentences"}
      />
      <Icon.Button name="add"
        style={ styles.addItemBtn }
        size={28}
        backgroundColor="rgba(0,0,0,0)"
        onPress={ this._addStore }/>
      </View>
    );
  }
}
