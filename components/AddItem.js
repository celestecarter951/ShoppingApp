import React, { Component } from 'react';
import { StyleSheet, View, Button, Keyboard, ToastAndroid } from 'react-native';
import styles from '../styles.js';
import db from '../db';
import Icon from 'react-native-vector-icons/MaterialIcons';
import TextField from 'react-native-md-textinput';

import _ from 'lodash';

export default class AddItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
        itemName: ''
    };

    this._addItem = this._addItem.bind(this);
  }

  _addItem = () => {
    if (this.state.itemName) {
      var itemName = this.state.itemName;
      itemInList = _.find(this.props.items, function(item) {
        return _.lowerCase(item.name) == _.lowerCase(itemName);
      });

      this._addItemToList(itemInList);
      this.setState({itemName: ''});
    }
    Keyboard.dismiss();
  }

  _addItemToList(itemInList) {
      if (itemInList) {
        if (itemInList.checked) {
          db.item.update(itemInList.name, {checked: false});
        } else {
          var message = "Item '" + itemInList.name + "' is already in the list";
          ToastAndroid.show(message, ToastAndroid.LONG);
        }
      } else {
        db.item.add(this.state.itemName);
      }
  }

  render() {
    let defaultText = 'OpenSans-Semibold';

    return(
      <View style={[ styles.rowFlex, styles.addItemView ]}>
      <TextField
        labelStyle={{ fontFamily: defaultText }}
        wrapperStyle={{ flex: 1 }}
        inputStyle={{ fontFamily: defaultText }}
        textColor={"white"}
        onChangeText={(text) => { this.state.itemName = text }}
        onSubmitEditing={ this._addItem }
        label={"Add item"}
        highlightColor={"#53DBD0"}
        maxLength={ 40 }
        value={ this.state.itemName }
        height={ 40 }
        autoCapitalize={"sentences"}
      />
      <Icon.Button name="add"
        style={ styles.addItemBtn }
        size={28}
        backgroundColor="rgba(0,0,0,0)"
        onPress={ this._addItem }/>
      </View>
    )
  }
}
