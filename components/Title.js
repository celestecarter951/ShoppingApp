import React, { Component } from 'react';
import { Text, View, Button } from 'react-native';
import styles from '../styles';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Title extends Component {
  render() {
    return (
      <View style={[ styles.title, styles.rowFlex ]}>
        <Icon.Button name="bars"
          style={{ marginTop: 8, marginLeft: 10 }}
          size={24}
          backgroundColor="rgba(0,0,0,0)"
          onPress={() => { this.props.openControlPanel() }}
        />
        <Text style={ styles.titleText }>{ this.props.text }</Text>
        <Icon.Button name="ellipsis-v"
          style={{ marginTop: 8, marginRight: 10 }}
          size={24}
          backgroundColor="rgba(0,0,0,0)"
          onPress={() => { this.props.listOptions() } }
        />
      </View>
    );
  }
}
