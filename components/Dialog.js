import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View } from 'react-native';
import styles from '../styles.js';

export default class EditModal extends Component {
  render() {
    return (
      <Modal
        transparent={ true }
        animationType={ 'slide' }
        onRequestClose={ this.props.close } >
        <View
          style={ styles.modal }>
          <View
            style={ styles.popup }>
            <View
              style={ styles.modalContent }>
              <Text style={ styles.modalHeader }>
                { this.props.title }
              </Text>
              { this.props.children }
            </View>
            <View style={ styles.modalButtons }>
              <TouchableHighlight style={ styles.modalBtn }
                onPress={ this.props.close }>
                <Text style={ styles.modalTextBtn }>
                  { this.props.cancelBtn }
                </Text>
              </TouchableHighlight>
              <TouchableHighlight style={ styles.modalBtn }
                onPress={ this.props.submit }>
                <Text style={ styles.modalTextBtn }>
                  { this.props.doneBtn }
                </Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
      </Modal>
    )
  }
}
