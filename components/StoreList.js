import React, { Component } from 'react';
import { View, Text, ListView, Button } from 'react-native';
import AddStore from './AddStore';

import styles from '../styles.js';

export default class StoreList extends Component {
  render() {
    let stores = Object.keys(this.props.stores)
      .map((id) => ({id , name: this.props.stores[id]}))
    let source = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    source = source.cloneWithRows(stores);

    return (
      <View>
        <AddStore />
        <ListView
          enableEmptySections={ true }
          dataSource={ source }
          renderRow={ (item, sectionId, rowId) =>
            <Text
              style={styles.menuItem}
              onPress={ () => this.props.storeView(item.id) }>
                {item.name}
            </Text> }
        />
      </View>
    );
  }
}
