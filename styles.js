import { StyleSheet } from 'react-native';

let background = '#1E1D23'; //rgba(31,28,35,1);
let foreground = '#25252F'; //rgba(38,37,47,1);
let gray = '#88878f';

let red = '#D93344'; //rgba(214,53,74,1);
let blue = '#5486D1'; //rgba(83,128,193,1);
let purple = '#9D4dC5'; //rgba(137,70,171,1);
let yellow = '#F4FC74'; //rgba(252,254,107,1);
let cyan = '#53DBD0'; //rgba(94,219,207,1);
let darkCyan = '#44A3AC'; //regba(68,163,172,1);

let defaultText = 'OpenSans-Semibold';
let boldText = 'OpenSans-Bold';
let headerText = 'YanoneKaffeesatz-Regular';
let fancyText = 'CoveredByYourGrace';

let styles = StyleSheet.create({
  defaultFont: {
    fontFamily: defaultText
  },
  boldFont: {
    fontFamily: boldText
  },
  mainView: {
    backgroundColor: background,
    flex: 1
  },
  titleText: {
    flex: 1,
    color: 'white',
    fontSize: 30,
    textAlign: 'center',
    padding: 7,
    marginRight: 40,
    fontFamily: fancyText
  },
  title: {
    backgroundColor: foreground,
    borderBottomColor: background,
    borderBottomWidth: 2,
    maxHeight: 55
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    paddingLeft: 10,
    margin: 2,
    backgroundColor: foreground,
  },
  itemText: {
    fontFamily: headerText,
    fontSize: 20,
    color: 'white'
  },
  itemCheckedText: {
    fontFamily: headerText,
    fontSize: 20,
    color: 'white',
    textDecorationLine: 'line-through'
  },
  rowFlex: {
    flexDirection: 'row'
  },
  itemDetailText: {
    color: gray,
    fontFamily: defaultText,
    fontSize: 12,
    width: 50
  },
  note: {
    fontFamily: defaultText,
    fontSize: 12,
    color: gray,
    flex: 1,
    height: 20
  },
  price: {
    color: gray,
    padding: 5,
    paddingRight: 10
  },
  quantityBox: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: blue
  },
  quantityBoxChecked: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: purple
  },
  quantity: {
    color: foreground,
    fontFamily: boldText,
    fontSize: 23
  },
  itemList: {
    flex: 1,
    padding: 10
  },
  space: {
    padding: 20
  },
  rowSpacing: {
    maxHeight: 50
  },
  dropdown: {
    flex: 1
  },
  editBtn: {
    width: 50,
    height: 50
  },
  input: {
    fontSize: 16,
    fontFamily: defaultText,
    textAlign: 'left',
  },
  quantity: {
    fontFamily: defaultText,
    fontSize: 20,
    flex: 1,
    paddingTop: 10,
    textAlign: 'center'
  },
  measurementAmount: {
    fontSize: 16,
    fontFamily: defaultText,
    textAlign: 'right',
    marginRight: 20,
  },
  pricePerMeasurement: {
    fontSize: 16,
    fontFamily: defaultText,
    color: 'white',
    marginTop: 15,
    marginBottom: 10
  },
  subBtn: {
    marginLeft: 50
  },
  addBtn: {
    marginRight: 50
  },
  menuItem: {
    backgroundColor: background,
    color: 'white',
    fontSize: 20,
    fontFamily: headerText,
    marginTop: 2,
    marginLeft: 5,
    marginRight: 5,
    padding: 10
  },
  addItemView: {
    paddingLeft: 10,
    paddingRight: 10,
  },
  addItemBtn: {
    marginTop: 30,
  },
  itemDetails: {
    flex: 1
  },
  storeIcons: {
    fontSize: 23,
    height: 26,
  },
  currencySign: {
    fontSize: 20,
    color: 'white',
    marginTop: 18
  },
  iconBtn: {
    margin: 5,
    fontSize: 30,
    width: 35
  },
  category: {
    backgroundColor: background,
    borderBottomColor: cyan,
    borderBottomWidth: 0.8,
    padding: 5,
    marginBottom: 5
  },
  check: {
    fontSize: 23,
    height: 26,
    color: cyan
  },
  categoryText: {
    fontFamily: headerText,
    fontSize: 23
  },
  modal: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
    justifyContent: 'center'
  },
  modalHeader: {
    marginBottom: 20,
    fontSize: 25,
    textAlign: 'left',
    fontFamily: headerText,
    color: 'white'
  },
  modalContent: {
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
  modalButtons: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    alignSelf: 'flex-end'
  },
  modalBtn: {
    minWidth: 64,
    height: 36,
    padding: 8,
    marginTop: 8,
    marginBottom: 8,
    marginRight: 8
  },
  modalTextBtn: {
    color: cyan,
    textAlign: 'center',
    fontFamily: headerText,
    fontSize: 18
  },
  popup: {
    backgroundColor: foreground,
    borderRadius: 5,
    marginHorizontal: 36
  },
  topButton: {
    flex: 1,
    paddingBottom: 10,
    paddingTop: 20,
    borderBottomWidth: 2
  },
  topButtonText: {
    fontFamily: headerText,
    fontSize: 20,
    textAlign: 'center',
  },
  listOptionsModal: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
  },
  popupList: {
    backgroundColor: foreground,
    borderRadius: 5,
    marginLeft: 160,
    marginTop: 45
  },
  listOption: {
    fontFamily: defaultText,
    fontSize: 15
  },
  horizontalLine: {
    borderBottomColor: cyan,
    borderBottomWidth: 1,
    marginTop: 10,
    marginBottom: 10
  },
  deleteItem: {
    borderBottomColor: purple
  },
  moveItem: {
    borderBottomColor: blue
  },
  copyItem: {
    borderBottomColor: darkCyan
  },

});

export default styles;
