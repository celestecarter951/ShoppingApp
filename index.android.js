import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import Drawer from 'react-native-drawer';
import ShoppingList from './components/ShoppingList.js';
import StoreList from './components/StoreList.js';

import db from './db';

class ShoppingApp extends Component {
  constructor(props) {
    super(props);

    db.on('change', this.update.bind(this));

    this.state = {
      index: 0,
      lists: {},
      categories: [],
      name: 'Loading...',
      items: {
      }
    }
  }

  storeView(id) {
    db.list.setActive(id);
    this.closeControlPanel();
  }

  update(nextState) {
    this.setState(nextState);
  }

  closeControlPanel = () => {
    this._drawer.close()
  };

  openControlPanel = () => {
    this._drawer.open()
  };

    render () {
        return (
          <Drawer
            type="overlay"
            styles={drawerStyles}
            tapToClose={true}
            openDrawerOffset={0.2} // 20% gap on the right side of drawer
            panCloseMask={0.2}
            closedDrawerOffset={-3}
            openDrawerOffset={150}
            ref={(ref) => this._drawer = ref}
            content={<StoreList
                      stores={this.state.lists}
                      storeView={this.storeView.bind(this)}/>}
            tweenHandler={(ratio) => ({
                mainOverlay: { opacity:(ratio - 0.5), backgroundColor: 'black' }
            })}
          >
            <ShoppingList {...this.state}
              index={this.state.index}
              openControlPanel={this.openControlPanel.bind(this)}
            />
          </Drawer>
    )
  }
}

const drawerStyles = {
  drawer: { backgroundColor: '#25252F' },
  main: {paddingLeft: 3},
}

AppRegistry.registerComponent('ShoppingApp', () => ShoppingApp);
