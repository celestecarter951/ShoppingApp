module.exports = {
  users: {
    userid1: {
      categories: {
        '': true,
        frozen: true
      },
      default: 'listid1',
      lists: {
        listid1: true,
        listid2: true,
        listid3: true
      }
    }
  },
  lists: {
    listid1: {
      name: 'walmart',
      owner: 'userid1',
      categories: ['produce', 'snacks', 'other', 'ice cream'],
      members: {
        userid1: true,
        userid2: true
      }
    }
  },
  items: {
    listid1: {
      'Oranges Juice': {
        price: '3.88',
        pricePerMeasurement: '0.95',
        measurement: 'lbs',
        note: 'Get the real stuff!',
        category: 'other',
        quantity: '12'
      },
      'Ground chicken': {
        price: '8.60',
        pricePerMeasurement: '2.95',
        measurement: 'oz',
        note: '',
        category: 'other',
        quantity: '1'
      },
      'Onion': {
        price: '1.20',
        pricePerMeasurement: '0.21',
        measurement: 'oz',
        note: '',
        category: 'produce',
        quantity: '1'
      },
      'Peas': {
        price: '4.58',
        pricePerMeasurement: '1.35',
        measurement: 'lbs',
        note: 'Frozen please',
        category: 'frozenVeges',
        quantity: '2'
      },
      'Bananas': {
        price: '0.73',
        pricePerMeasurement: '0.15',
        measurement: 'oz',
        note: 'I like the short stubby ones',
        category: 'produce',
        quantity: '2'
      },
      'Romain Lettuce': {
        price: '1.60',
        pricePerMeasurement: '0.25',
        measurement: 'oz',
        note: 'Check for bruising',
        category: 'produce',
        quantity: '1'
      },
      'Almond Milk Unsweetened: Great Value Brand is cheaper': {
        price: '1.60',
        pricePerMeasurement: '0.25',
        measurement: 'oz',
        note: 'This is a very long note meant for those who don\'t understand what milk I get',
        category: 'other',
        quantity: '99'
      }
    }
  }
}
