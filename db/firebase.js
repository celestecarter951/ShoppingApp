import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyAaenu5fagLZiadOUr2D5GATsf5vcst6VU",
  authDomain: "learnfirebase-eb9b0.firebaseapp.com",
  databaseURL: "https://learnfirebase-eb9b0.firebaseio.com",
  storageBucket: "learnfirebase-eb9b0.appspot.com",
  messagingSenderId: "380667939024"
};

firebase.initializeApp(config);

export default firebase;
