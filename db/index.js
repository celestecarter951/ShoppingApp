import Promise from 'bluebird';
import events from 'events';

import firebase from './firebase';
import mock from '../mock/list';
import _ from 'lodash';

let api = new events.EventEmitter();
let db = firebase.database();

let fakeDbRef = {
  off: function () {
  }
};

let cache = {
  uid: '',
  items: {
  },
  lists: {
    active: '',
    metadata: {
    }
  },
  listeners: {
    // id of the active list
    active: fakeDbRef,
    // All items on all lists
    items: {},
    // id of available lists
    lists: fakeDbRef,
    // name of active list
    activeMeta: fakeDbRef,
    // names of all lists
    meta: {},
    // list of all categories associated with the user
    categories: fakeDbRef
  }
};

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    handleUser(user);
    initialListeners();
  } else {
    firebase.auth()
      .signInAnonymously()
      .then(handleUser)
      .then(createNewUser)
      .then(initialListeners);
  }
});

function createNewUser () {
  let id = cache.uid;
  let listId = db.ref().child('lists').push().key;
  let initialData = {};
  let user = {};

  user[id] = true;

  initialData[`users/${ id }`] = {
    active: listId,
    lists: {
    }
  };

  initialData[`users/${ id }`].lists[listId] = true;

  initialData[`lists/${ listId }`] = {
    name: 'First list',
    owner: id,
    members: user
  };

  initialData[`items/${ listId }`] = {
    'Oranges Juice': {
      price: '3.88',
      pricePerMeasurement: '0.95',
      measurement: 'lbs',
      note: 'Get the real stuff!',
      category: 'other',
      quantity: '12'
    }
  };

  return db.ref().update(initialData);
}

function itemsChanged (listId) {
  if (listId == cache.lists.active) {
    let items = _.get(cache.items, listId);
    // default for value null
    items = items || {};
    api.emit('change', { items });
  }
}

function activeMetadata (snapshot) {
  let name = _.get(snapshot.val(), 'name', '');

  api.emit('change', { name });
}

function updateListName (snapshot) {
  let name = snapshot.val().name;
  cache.lists.metadata[this.id] = name;

  api.emit('change', { lists: cache.lists.metadata });
}

function updateItemCache (snapshot) {
  let items = snapshot.val();
  cache.items[this.id] = items;
  itemsChanged(this.id);
}

function initialListeners () {
  cache.listeners.active = db.ref(`users/${ cache.uid }/active`);
  cache.listeners.active.on('value', function (snapshot) {
    let id = snapshot.val();

    cache.lists.active = id;

    cache.listeners.activeMeta.off();
    cache.listeners.activeMeta = db.ref(`lists/${ id }`);
    cache.listeners.activeMeta.on('value', activeMetadata);
    itemsChanged(id);
  });

  cache.listeners.lists = db.ref(`users/${ cache.uid }/lists`);
  cache.listeners.lists.on('value', function (snapshot) {
    let lists = Object.keys(snapshot.val());

    lists.forEach((id) => {
      if (!cache.listeners.meta[id]) {
        cache.listeners.meta[id] = db.ref(`lists/${ id }`);
        cache.listeners.meta[id].on('value', updateListName.bind({ id }));
      }
      if (!cache.listeners.items[id]) {
        cache.listeners.items[id] = db.ref(`items/${ id }`);
        cache.listeners.items[id].on('value', updateItemCache.bind({ id }));
      }
    });
  });

  cache.listeners.categories = db.ref(`users/${ cache.uid }/categories`);
  cache.listeners.categories.on('value', function (snapshot) {
    let cats = snapshot.val();

    if (cats) {
      api.emit('change', { categories: Object.keys(cats) });
    }
  });
}

function switchActive (id) {
  return cache.listeners.active.set(id);
}

function addItem (name) {
  let item = {};
  let props = {
    category: 'other',
    checked: false,
    measurement: '',
    measurementAmount: '1',
    note: '',
    price: '',
    quantity: '1'
  };

  item[name] = props;
  return cache.listeners.items[cache.lists.active].update(item);
}

function editListName(name) {
  return cache.listeners.activeMeta.update({name});
}

function addCategory(name) {
  let category = {};
  category[name] = true;

  return cache.listeners.categories.update(category);
}

function createList(name) {
  let id = cache.uid;
  let listId = db.ref().child('lists').push().key;
  let listMeta = {};
  let user = {};
  user[id] = true;

  listMeta[`lists/${ listId }`] = {
    name,
    owner: id,
    members: user
  };

  let userLists = {};
  userLists[listId] = true;


  return db.ref().update(listMeta)
    .then(() => cache.listeners.lists.update(userLists));
}

function deleteActive() {
  let id = cache.uid;
  let active = cache.lists.active;
  let deletes = {};

  deletes[`items/${ active }`] = null;
  deletes[`lists/${ active }`] = null;
  deletes[`users/${ id }/lists/${ active }`] = null;

  cache.listeners.activeMeta.off();
  cache.listeners.activeMeta = fakeDbRef;
  cache.listeners.meta[active].off();

  delete cache.listeners.meta[active];
  delete cache.lists.metadata[active];

  cache.listeners.items[active].off();
  delete cache.listeners.items[active];

  return db.ref().update(deletes)
    .then(() => {
      let lists = _.keys(cache.listeners.meta);

      if (lists.length > 0) {
        return switchActive(lists[0]);
      }
      return Promise.resolve();
    });
}

function uploadMock () {
  return db.ref('/')
    .set(mock);
}

function updateItem (name, change) {
  let active = cache.lists.active;

  return db.ref(`items/${ active }/${ name }`).update(change);
}

function renameItem (from, to) {
  let active = cache.lists.active;

  return db.ref(`items/${ active }/${ from }`)
    .once('value', (snapshot) => {
      let props = snapshot.val();

      return db.ref(`items/${ active }/${ to }`)
        .update(props)
        .then(() => db.ref(`items/${ active }/${ from }`).remove());
    });
}

function deleteItem (itemName) {
    let active = cache.lists.active;
    return db.ref(`items/${ active }/${ itemName }`).remove();
}

function copyItem (item, toList) {
  let name = item.name;
  let props = _.pick(item, [
    'category',
    'measurement',
    'measurementAmount',
    'note',
    'price',
    'quantity'
  ]);

  props.checked = false;

  return db.ref(`items/${ toList }/${ name }`).update(props);
}

function handleUser (user) {
  cache.uid = user.uid;
}

api.list = {
  setActive: switchActive,
  delete: deleteActive,
  update: {
    name: editListName
  },
  add: createList
};

api.item = {
  add: addItem,
  copy: copyItem,
  delete: deleteItem,
  rename: renameItem,
  update: updateItem
};

api.categories = {
  add: addCategory
};

api.cached = {
  list: function (id) {
    return {
      items: function () {
        return cache.items[id];
      }
    };
  }
};

export default api;
